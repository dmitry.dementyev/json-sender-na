const router = require('express').Router()
const fs = require('fs')


router.get('/baby-profiles', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/baby-profiles.json'));
    res.send(data)
})
router.get('/food-items-nutrifacts', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/food-items-nutrifacts.json'));
    res.send(data)
})
router.get('/food-items', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/food-items.json'));
    res.send(data)
})
router.get('/nutrients-meta', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/nutrients-meta.json'));
    res.send(data)
})
router.get('/nutrilini-recipe-cards', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/nutrilini-recipe-cards.json'));
    res.send(data)
})
router.get('/nutrilini-recipes-nutrifacts', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/nutrilini-recipes-nutrifacts.json'));
    res.send(data)
})
router.get('/nutrilini-recipes', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/nutrilini-recipes.json'));
    res.send(data)
})
router.get('/optidiet-recipes-nutrifacts', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/optidiet-recipes-nutrifacts.json'));
    res.send(data)
})
router.get('/optidiet-recipes', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/optidiet-recipes.json'));
    res.send(data)
})
router.get('/profiles', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/profiles.json'));
    res.send(data)
})
router.get('/rich-in-thresholds', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/rich-in-thresholds.json'));
    res.send(data)
})
router.get('/young-mom-profiles', function (req, res) {
    let data = JSON.parse(fs.readFileSync('jsons/young-mom-profiles.json'));
    res.send(data)
})
router.get('/food-photo', function (req, res) {
    const sharp = require('sharp');
    var fs = require('fs');

    function readFiles(dirname, onFileContent, onError) {
        fs.readdir(dirname, function (err, filenames) {
            if (err) {
                onError(err);
                return;
            }

            filenames.forEach(function (filename) {
                fs.readFile(dirname + filename, function (err, content) {
                    if (err) {
                        onError(err);
                        return;
                    }
                    onFileContent(filename, content);
                });
            });
        });
    }
    readFiles('food-resized/', async function (filename, content) {
        if(filename.split('.')[0] ==req.query['key'] ){

            res.type(filename.split('.')[1])
            res.send(content)
            return
        }
    }, function (err) {
        throw err;
    });
  

   
})
module.exports = router