const express = require('express')
const app = express()

// app.use(express.static('public'));
app.use(express.static('public')); 
app.use('/images', express.static('food-resized/'))
const conf = require('./conf')
const routes = require('./routes')

const port = conf.PORT || 3000

console.log(port);

app.use('/api', routes)

app.listen(port, () => {
    console.log("Server started")
})